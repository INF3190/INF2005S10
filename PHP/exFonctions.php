<?php
include 'head.php';//entete du fichier html

function evenement($nom,$organisateur){
    ?>
    <table class="table">
    <caption class="text-center">Information sur l'événement</caption>
  <tr>
    <th>Nom de l'évenement</th>
    <th>Organisateur</th>
  </tr>
  <tr>
    <td><?=$nom; ?></td>
    <td><?=$organisateur; ?></td>
  </tr>
</table>
    <?php     
}

//fonction avec nombre arguments variable
function membres(...$liste){
    echo "<h3>Liste des membres</h3>";
    echo "<ul>";
    foreach ($liste as $elem){
        echo "<li>$elem</li>";
    }
    echo "</ul>";
}

//autre fonction avec nombre arguments variable
function inscriptions($activite, ...$invites) {
    echo "<h2>Invites à l'activité $activite</h2>";
    echo "<ol>";
    for($i=0; $i<count($invites);$i++){
        echo "<li> ".$invites[$i] ." </li>";
    }
    echo "</ol>";
}

// Exemples appels des fonctions.
evenement('Conférence','UQAM');
membres("Jean");
membres("Jean","Marc","Luc");
inscriptions("Vente publique","Jean");
inscriptions("Vente publique", "Marie", "Jean", "Mathieu");


include 'tail.php';