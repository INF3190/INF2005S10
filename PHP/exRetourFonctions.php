<?php
include 'head.php';
// retourner une valeur simple.
function dateEvenement($nom,$organisateur){
    $returnval = "";
    if(!empty($nom)){
        $returnval = date("Y-m-d ") .": $nom";
    }
    return ($returnval);
}

// retourner un tableau.
function nomsMembres(...$liste){
    $jeton='jeton';
    $returnval = array();
    for($i=0; $i<count($liste); $i++){
        $cle = $jeton. $i; 
        $returnval[$cle]= $liste[$i];
    }
    return ($returnval);
}

// retourner un json.
function participants($activite, ...$invites) {
    $returnval = array();
    $cleActivite='activite';
    $cleInvite='invite';
    foreach ($invites as $invite){
        $returnval[]=array($cleActivite=>$activite,
            $cleInvite=>$invite);
    }
    return json_encode($returnval);
}

// exemples appels

echo "<h3>Événements - chaine</h3>";
echo "<pre>";
$dateEvent = dateEvenement('Conférence','UQAM');
echo $dateEvent;
echo "</pre>";
echo "<h3>Membres - tableau</h3>";
echo "<pre>";
$memb1 = nomsMembres("Jean");
print_r($memb1);
echo "</pre>";
echo "<pre>";
$memb2 = nomsMembres("Jean","Marc","Luc");
print_r($memb2);
echo "</pre>";
echo "<h3>Participants - json</h3>";
echo "<pre>";
echo participants("Vente publique","Jean");
echo "</pre>";
echo "<pre>";
echo participants("Vente publique", "Marie", "Jean", "Mathieu");
echo "</pre>";
include 'tail.php';